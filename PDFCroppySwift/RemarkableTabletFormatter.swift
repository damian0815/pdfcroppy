//
//  RemarkableTabletFormatter.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 08/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Foundation
import Quartz

let kMmToPoints : CGFloat = 1.0/0.35277777777778

class RemarkableTabletFormatter {

    static let kHUIMarginMM: CGFloat = 11
    static let kRmHeightMM: CGFloat = 256
    static let kRmWidthMM: CGFloat = 177
    static let kRmAspectRatio = kRmWidthMM/kRmHeightMM

    var hInsetMm : CGFloat = kHUIMarginMM
    var writingMarginSizeMm : CGFloat = 50
    
    
    var blankWritingArea : Bool = true

    enum Orientation {
        case portrait
        case landscape
    }
    
    var orientation : Orientation = .landscape
    
    func format(doc: PDFDocument) -> PDFDocument {

        let data = CFDataCreateMutable(kCFAllocatorDefault, 0)!
        let consumer = CGDataConsumer(data: data)!

        var (mediaBox, contentBox, writingAreaBox) = getMediaBoxContentBoxAndWritingAreaBox()

        let pdfContext = CGContext(consumer: consumer, mediaBox: &mediaBox, nil)!
        let graphicsContext = NSGraphicsContext(cgContext: pdfContext, flipped: false)
        
        NSGraphicsContext.current = graphicsContext
        
        
        for pageNum in 0..<doc.pageCount {
            pdfContext.beginPDFPage(nil);
            
            let page = doc.page(at: pageNum)!
            let pageCropBoxSize = getOrientedSize(page, for: .cropBox)
            
            let (translation, scale) = RectUtil.translateAndScaleToFit(sourceSize: pageCropBoxSize, targetRect: contentBox, centerMode: .vertical)
            
            pdfContext.saveGState()
            pdfContext.translateBy(x: translation.x, y: translation.y)
            pdfContext.scaleBy(x: scale, y: scale)
            page.draw(with: .cropBox, to: pdfContext)
            pdfContext.restoreGState()
            
            if self.blankWritingArea {
                pdfContext.setFillColor(CGColor.white)
                pdfContext.fill(writingAreaBox)
            }
            
            pdfContext.endPDFPage()
        }
        
        NSGraphicsContext.current = nil
        pdfContext.closePDF()
        
        return PDFDocument(data: data as Data)!
        
    }
    
    func getMediaBoxContentBoxAndWritingAreaBox() -> (CGRect, CGRect, CGRect) {
        // 1 point = 0.35277777777778 mm
        // -> 1 mm = 1/0.352777777778 points
        let mediaBox = RemarkableTabletFormatter.getMediaBox(orientation: orientation)
        let contentBox = getContentBox(mediaBox: mediaBox)
        let writingAreaBox = getWritingAreaBox(mediaBox: mediaBox)
        
        return (mediaBox, contentBox, writingAreaBox)
    }
    
    func getContentBox(mediaBox: CGRect) -> CGRect {
        let hInsetPoints = hInsetMm*kMmToPoints
        let writingAreaBox = getWritingAreaBox(mediaBox: mediaBox)
        let contentBox = CGRect(x:hInsetPoints, y:0, width: mediaBox.width-hInsetPoints-writingAreaBox.size.width, height: mediaBox.height)
        return contentBox;
    }
    
    func getWritingAreaBox(mediaBox: CGRect) -> CGRect {
        let writingMarginSizePoints = writingMarginSizeMm*kMmToPoints
        let writingAreaBox = CGRect(x:mediaBox.width-writingMarginSizePoints, y:0, width:writingMarginSizePoints, height:mediaBox.height)
        return writingAreaBox
    }
    
    static func getMediaBox(orientation: Orientation) -> CGRect {
        if orientation == .landscape {
            return CGRect(x:0, y:0, width: kRmHeightMM*kMmToPoints, height: kRmWidthMM*kMmToPoints)
        } else {
            return CGRect(x:0, y:0, width: kRmWidthMM*kMmToPoints, height: kRmHeightMM*kMmToPoints)
        }
    }

    func getBestOrientation(doc: PDFDocument) -> Orientation {
        
        if doc.pageCount == 0 {
            return .portrait
        }
    
        let mediaBoxLandscape = RemarkableTabletFormatter.getMediaBox(orientation: .landscape)
        let mediaBoxPortrait = RemarkableTabletFormatter.getMediaBox(orientation: .portrait)
        let contentBoxLandscape = getContentBox(mediaBox: mediaBoxLandscape)
        let contentBoxPortrait = getContentBox(mediaBox: mediaBoxPortrait)

        // determine median page scaling for landscape and portrait orientations

        var scalesPortrait = Array<CGFloat>()
        var scalesLandscape = Array<CGFloat>()
        
        for pageNum in 0..<doc.pageCount {

            let page = doc.page(at: pageNum)!
            let pageCropBoxSize = getOrientedSize(page, for: .cropBox)
            
            let (_, scaleLandscape) = RectUtil.translateAndScaleToFit(sourceSize: pageCropBoxSize, targetRect: contentBoxLandscape, centerMode: .vertical)
            let (_, scalePortrait) = RectUtil.translateAndScaleToFit(sourceSize: pageCropBoxSize, targetRect: contentBoxPortrait, centerMode: .vertical)
            
            scalesLandscape.append(scaleLandscape)
            scalesPortrait.append(scalePortrait)
        }
        
        scalesLandscape.sort()
        scalesPortrait.sort()
        
        let medianLandscapeScale = scalesLandscape[scalesLandscape.count/2]
        let medianPortraitScale = scalesPortrait[scalesPortrait.count/2]

        print("median scales: landscape",medianLandscapeScale,"   portrait",medianPortraitScale)
        
        if medianLandscapeScale > medianPortraitScale {
            return .landscape
        } else {
            return .portrait
        }
        
        
    }
    
    func getOrientedSize(_ page: PDFPage, for: PDFDisplayBox) -> CGSize {
        let size = page.bounds(for: .cropBox).size
        if page.rotation == 270 || page.rotation == 90 {
            return CGSize(width: size.height, height: size.width)
        } else {
            return size
        }
    }

}
