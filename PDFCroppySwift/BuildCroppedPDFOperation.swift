//
//  CroppedPDFBuilder.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 07/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Foundation
import Quartz

class BuildCroppedPDFOperation: Operation {

	private(set) var result: PDFDocument? = nil
    private let source: PDFDocument
	private let cropBoxRelativeRects: [CGRect]
    
    init(document: PDFDocument, cropBoxRelativeRects: [CGRect]) {
        self.source = document.copy() as! PDFDocument
		self.cropBoxRelativeRects = cropBoxRelativeRects
    }
	
	override func main() {
		self.result = buildCroppedPdf()
	}
	
    private func buildCroppedPdf() -> PDFDocument? {
		return BuildCroppedPDFOperation.buildCroppedPdf(from: self.source, cropBoxRelativeRects: self.cropBoxRelativeRects, isCancelled: { return self.isCancelled })
	}
	
	public static func buildCroppedPdf(from source: PDFDocument, cropBoxRelativeRects: [CGRect], isCancelled: () -> Bool) -> PDFDocument? {
		
        let result = PDFDocument()
        
        let onlyAddPage2 = false
        
        for pageNum in 0..<source.pageCount {
			if isCancelled() {
				print("cancelled")
				return nil
			}
            for cropBoxRelativeRect_Raw in cropBoxRelativeRects {
                let cropBoxRelativeRect = CGRect(x: floor(cropBoxRelativeRect_Raw.minX),
                                                 y: floor(cropBoxRelativeRect_Raw.minY),
                                                 width: floor(cropBoxRelativeRect_Raw.width),
                                                 height: floor(cropBoxRelativeRect_Raw.height))
                let page = source.page(at: pageNum)?.copy() as! PDFPage
                let cropBox = page.bounds(for: PDFDisplayBox.cropBox)
                if onlyAddPage2 && pageNum != 2 {
                    continue
                }
                
                let newCropBox = BuildCroppedPDFOperation.getNewCropBox(originalCropBox:cropBox,
                                               cropBoxRelativeRect:cropBoxRelativeRect,
                                               rotation:page.rotation)
                print("page", pageNum, "has rotation", page.rotation, "and original crop box", cropBox, "modified", newCropBox)

                page.setBounds(newCropBox, for: PDFDisplayBox.cropBox)
                page.setBounds(newCropBox, for: PDFDisplayBox.mediaBox)
                
                result.insert(page, at: result.pageCount)
            }
        }
        
        return result
    }
    
    private static func getNewCropBox(originalCropBox: CGRect, cropBoxRelativeRect:CGRect, rotation: Int) -> CGRect {
        
        var cropBox = originalCropBox
        

        switch rotation {
        case 0: // correct
            cropBox.origin.x += cropBoxRelativeRect.origin.x
            cropBox.origin.y += cropBoxRelativeRect.origin.y
            cropBox.size = cropBoxRelativeRect.size
            return cropBox
        case 180: // correct
            cropBox.origin.x += (cropBox.width - cropBoxRelativeRect.maxX)
            cropBox.origin.y += (cropBox.height - cropBoxRelativeRect.maxY)
            cropBox.size = cropBoxRelativeRect.size
            return cropBox
        case 90: // correct
            cropBox.origin.x += (cropBox.width - cropBoxRelativeRect.maxY)
            cropBox.origin.y += cropBoxRelativeRect.origin.x
            cropBox.size.width = cropBoxRelativeRect.size.height
            cropBox.size.height = cropBoxRelativeRect.size.width
            return cropBox
        case 270:
            cropBox.origin.x += cropBoxRelativeRect.origin.y;
            cropBox.origin.y += (cropBox.height - cropBoxRelativeRect.maxX)
            cropBox.size.width = cropBoxRelativeRect.size.height
            cropBox.size.height = cropBoxRelativeRect.size.width
            return cropBox
        default:
            print("Unsupported rotation:", rotation)
            return cropBox
        }
    }
}
