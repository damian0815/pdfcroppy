//
//  ViewController.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 03/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Cocoa
import Quartz

class RootViewController: NSSplitViewController {
    
    static var instance : RootViewController? = nil
    
    let formatter = RemarkableTabletFormatter()
    
    var outputUrl : URL? = nil
    var loadedUrl : URL? = nil
    var formattedPdf : PDFDocument? = nil
    
    override init(nibName nibNameOrNil: NSNib.Name?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        assert(RootViewController.instance == nil)
        RootViewController.instance = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        assert(RootViewController.instance == nil)
        RootViewController.instance = self
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.		
		self.getPdfView().autoScales = true
		
		
        
        getMergedPagesViewController().onCroppedPdfRebuilt = {
            croppedPdf, makeToolstripMargin, writingAreaWidth in
            
            self.formatter.hInsetMm = makeToolstripMargin ? RemarkableTabletFormatter.kHUIMarginMM : 0
            self.formatter.writingMarginSizeMm = CGFloat(writingAreaWidth)

            let bestOrientation = self.formatter.getBestOrientation(doc: croppedPdf)
            self.formatter.orientation = bestOrientation
            
            self.formattedPdf = self.formatter.format(doc: croppedPdf)
            let pdfView = self.getPdfView();
            pdfView.document = self.formattedPdf
            
            self.saveFormattingDetailsToUserDefaults()
        }
        
        //let pdfFilename = "2_juul_rules_and_fiction-split.pdf"
        //let pdfFilename = "3_murray_from_additive_to_expressive_form.pdf"
        //let url = URL(fileURLWithPath: pdfFilename, relativeTo: Bundle.main.resourceURL)
        //self.loadPdf(url: url)
    }
    
    
    let kPerDocumentFormattingDetailsKey = "PerDocumentFormattingDetails"

    func saveFormattingDetailsToUserDefaults() {
        
        let mpvc = getMergedPagesViewController()
        
        let detailsDict = [
            "selectionRects": RootViewController.encodeSelectionRects(mpvc.mergedPagesView.selectionRects),
            "twoUp": String(mpvc.twoUpButton!.intValue),
            "hMargin": String(mpvc.avoidToolstripCheckBox!.intValue),
            "rotate": String(mpvc.rotateCheckbox!.intValue),
            "writingAreaSize": String(mpvc.writingAreaWidthSlider!.floatValue),
            ] as [String : Any]

		let loadedUrlString = self.loadedUrl!.absoluteString
		print("details for \(loadedUrlString): \(detailsDict)")
		
        let defaults = UserDefaults.standard
        
        var dict = defaults.dictionary(forKey: kPerDocumentFormattingDetailsKey)
        if dict == nil {
            dict = [:]
        }
        dict![loadedUrlString] = detailsDict
		
		
        defaults.set(dict, forKey: kPerDocumentFormattingDetailsKey)
        defaults.synchronize()
        
    }
    
    func tryLoadFormatingDetailsFromUserDefaults() {
        let defaults = UserDefaults.standard
        
        let dict = defaults.dictionary(forKey: kPerDocumentFormattingDetailsKey)
        if dict == nil {
            return
        }
        let detailsDict = dict![self.loadedUrl!.absoluteString] as? [ String : Any ]
        if detailsDict == nil {
            return
        }
        
        let mpvc = getMergedPagesViewController()

        mpvc.mergedPagesView.selectionRects = RootViewController.decodeSelectionRects(detailsDict!["selectionRects"] as! [String : Any])
        mpvc.twoUpButton!.intValue = Int32(detailsDict!["twoUp"] as! String)!
        mpvc.avoidToolstripCheckBox!.intValue = Int32(detailsDict!["hMargin"] as! String)!
        mpvc.writingAreaWidthSlider!.floatValue = Float(detailsDict!["writingAreaSize"] as! String)!
        
        let rotateCheckboxVal = detailsDict!["rotateCheckbox"]
        if rotateCheckboxVal != nil {
            mpvc.rotateCheckbox!.intValue = Int32(rotateCheckboxVal as! String)!
        } else {
            mpvc.rotateCheckbox!.intValue = 0
        }
    }

    static func decodeSelectionRects(_ dict: [String : Any]) -> [CGRect] {
        let rect0 = decodeRect(dict["rect0"] as! [String : Any])
        if dict["rect1"] == nil {
            return [rect0]
        }
        let rect1 = decodeRect(dict["rect1"] as! [String : Any])
        return [rect0, rect1]
    }
    
    static func encodeSelectionRects(_ rects: [CGRect]) -> [String : Any] {
        var encoded = [:] as [String : Any]
        encoded["rect0"] = encodeRect(rects[0])
        if rects.count > 1 {
            encoded["rect1"] = encodeRect(rects[1])
        }
        return encoded
    }
    
    static func encodeRect(_ r: CGRect) -> [String : Any] {
        return ["x":String(describing: r.minX),
                "y":String(describing: r.minY),
                "w":String(describing: r.width),
                "h":String(describing: r.height)]
    }
    
    static func decodeRect(_ d: [String : Any]) -> CGRect {
        return CGRect(x: CGFloat((d["x"] as! NSString).floatValue),
                      y: CGFloat((d["y"] as! NSString).floatValue),
                      width: CGFloat((d["w"] as! NSString).floatValue),
                      height: CGFloat((d["h"] as! NSString).floatValue))
    }
    
    static func findPageNumber(_ page:PDFPage, in doc:PDFDocument) -> Int {
        for pageNumber in 0..<doc.pageCount {
            if doc.page(at: pageNumber)!.isEqual(page) {
                return pageNumber
            }
        }
        return 0
    }
    

    func loadPdf(url: URL) {
        let pdfDoc = PDFDocument(url: url)!
        self.loadedUrl = url
		self.view.window!.title = "PDF Croppy - " + url.lastPathComponent
        self.outputUrl = nil
        getMergedPagesViewController().setPdfDocument(doc: pdfDoc)
        self.tryLoadFormatingDetailsFromUserDefaults()
        getMergedPagesViewController().propagateSliderValues()
        getMergedPagesViewController().cropPdf()
    }
    
    func saveCroppedPdf(url: URL) {
        self.formattedPdf!.write(to: url)
    }
    
    func getMergedPagesViewController() -> MergedPagesViewController {
        return self.splitViewItems[0].viewController as! MergedPagesViewController
    }
    
    func getPdfView() -> PDFView {
        return self.splitViewItems[1].viewController.view as! PDFView
    }
    
    func openDocument(callback: @escaping ((URL?) -> Void)) {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = false
        openPanel.allowedFileTypes = ["pdf"]
        openPanel.title = "Select a PDF file to open"
        openPanel.begin { response in
            if response.rawValue == 1 {
                let url = openPanel.urls[0]
                self.loadPdf(url: url)
            }
            callback(response.rawValue == 1 ? openPanel.urls[0] : nil)
        }
    }
    
    func saveDocument(callback: @escaping ((Bool) -> Void)) {
        if self.formattedPdf == nil {
            callback(false)
        } else if self.outputUrl != nil {
            self.saveCroppedPdf(url: self.outputUrl!)
            callback(true)
        } else {
            saveDocumentAs(callback: callback)
        }
    }
    
    func saveDocumentAs(callback: @escaping ((Bool) -> Void)) {
        if self.formattedPdf == nil {
            callback(false)
            return
        }
        let savePanel = NSSavePanel()
        savePanel.allowedFileTypes = ["pdf"]
        savePanel.title = "Save cropped PDF to"
		if let loadedUrl = loadedUrl {
			savePanel.nameFieldStringValue = loadedUrl.deletingPathExtension().lastPathComponent + "-noted.pdf"
			savePanel.directoryURL = loadedUrl.deletingLastPathComponent()
		}
        savePanel.begin { response in
            if response.rawValue == 1 {
                let url = savePanel.url
                self.outputUrl = url
                self.saveCroppedPdf(url: self.outputUrl!)
            }
            callback(response.rawValue == 1)
        }
    }

    
}

