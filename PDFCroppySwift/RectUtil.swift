//
//  RectUtil.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 08/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Foundation

class RectUtil {

    static func clampRect(_ rect:CGRect, bounds:CGRect) -> CGRect {
        let kMinSize = CGFloat(20)
        var origin = rect.origin
        var size = rect.size
        
        origin.x = min(max(0,origin.x),bounds.size.width-kMinSize)
        origin.y = min(max(0,origin.y),bounds.size.height-kMinSize)
        
        size.width = min(max(kMinSize,size.width), bounds.size.width - origin.x)
        size.height = min(max(kMinSize,size.height), bounds.size.height - origin.y)
        
        return CGRect(origin: origin, size: size)
    }
    
    static func translateAndScaleToFit(sourceSize: CGSize, targetSize: CGSize) -> (CGPoint, CGFloat) {
        return translateAndScaleToFit(sourceSize: sourceSize, targetRect: CGRect(origin: CGPoint.zero, size: targetSize), centerMode: .horizontalAndVertical);
    }
    
    enum CenterMode {
        case none
        case horizontal
        case vertical
        case horizontalAndVertical
    }
    
    static func translateAndScaleToFit(sourceSize: CGSize, targetRect: CGRect, centerMode: CenterMode) -> (CGPoint, CGFloat) {
        
        let targetSize = targetRect.size
        let imageAspect = sourceSize.width / sourceSize.height
        let viewAspect = targetSize.width / targetSize.height
        var translation = CGPoint.zero
        var scale = CGFloat(1.0)
        if (imageAspect > viewAspect) {
            // bound horizontally
            translation.x = 0
            scale = targetSize.width / sourceSize.width
            if centerMode == .vertical || centerMode == .horizontalAndVertical {
                translation.y = (targetSize.height - (scale * sourceSize.height))/2
            }
        } else {
            // bound vertically
            translation.y = 0
            scale = targetSize.height / sourceSize.height
            if centerMode == .horizontal || centerMode == .horizontalAndVertical {
                translation.x = (targetSize.width - (scale * sourceSize.width))/2
            }
        }
        
        translation.x += targetRect.origin.x
        translation.y += targetRect.origin.y
        return (translation, scale)
    }

}
