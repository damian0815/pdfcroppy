//
//  AppDelegate.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 03/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var saveDocument: NSMenuItem!
    
    @IBAction func onOpenDocument(_ sender: Any) {
        RootViewController.instance?.openDocument(callback: {
            url in
            
            if url != nil {
                NSDocumentController.shared.noteNewRecentDocumentURL(url!)
                self.saveDocument.title = "Save..."
                self.saveDocument.isEnabled = true
            }
        })
    }
    
    @IBAction func onSaveDocument(_ sender: Any) {
        RootViewController.instance?.saveDocument(callback: {
            response in
            
            if response {
                self.saveDocument.title = "Save"
            }
        })
    }
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        self.saveDocument.isEnabled = false
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    

}

