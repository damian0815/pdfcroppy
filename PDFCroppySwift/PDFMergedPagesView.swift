//
//  PDFMergedPagesView.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 03/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Cocoa
import Quartz
import CoreGraphics

class PDFMergedPagesView: NSView {
    
    var onSelectionRectChanged : (() -> Void)?
    
    var document: PDFDocument
    var image: NSImage?
    let backgroundColor: CGColor

    var activeSelectionRect: Int = -1
    var selectionRects: [CGRect]
    var writingAreaMm: CGFloat = 0 {
        didSet {
            self.needsDisplay = true
        }
    }
    var hMarginMm: CGFloat = 0 {
        didSet {
            self.needsDisplay = true
        }
    }
    
    enum DraggingEdge {
        case none
        case top
        case right
        case bottom
        case left
        case topLeft
        case bottomLeft
        case topRight
        case bottomRight
    }
    var draggingEdge: DraggingEdge
    
    func setPdfDocument(doc: PDFDocument) {
        self.document = doc
        if let cgImage = PDFMergedPagesView.makeImageForDocument(doc) {
            self.image = NSImage(cgImage:cgImage, size:NSZeroSize)
            self.setNeedsDisplay(self.bounds)
        } else {
            self.image = nil
        }
        let numSelectionRects = self.selectionRects.count
        self.selectionRects.removeAll()
        setNumSelectionRects(numSelectionRects)
    }
    
    required init?(coder decoder: NSCoder) {
        self.backgroundColor = CGColor.init(gray: 0.5, alpha: 1)
        self.selectionRects = [CGRect.zero]
        self.document = PDFDocument()
        self.draggingEdge = DraggingEdge.none
        super.init(coder:decoder)

		//self.registerForDraggedTypes([.string])
		
    }
	
	/*override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
		//let allow = shouldAllowDrag(sender)
		print("dragging entered: \(sender)")
		let allow = true
		return allow ? .copy : NSDragOperation()
	}*/
    
    func setNumSelectionRects(_ numRects: Int) {
        
        if let image = self.image {
            self.selectionRects.removeAll()
            for i in 0..<numRects {
                let space = image.size.width / CGFloat(numRects)
                let width = space
                
                let rect = CGRect(x: CGFloat(i) * space,
                                  y: 0,
                                  width: width,
                                  height: image.size.height)
                    .insetBy(dx: 20, dy: 20)
                self.selectionRects.append(rect)
            }
        }
        self.needsDisplay = true
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
        let viewContext = NSGraphicsContext.current!.cgContext
        viewContext.setFillColor(self.backgroundColor)
        viewContext.fill(dirtyRect)
        
        let (translation, scale) = getTranslateAndScale()
        
        viewContext.translateBy(x: translation.x, y: translation.y)
        viewContext.scaleBy(x: scale, y: scale)

        if let image = self.image {
            image.draw(at: CGPoint.zero, from: CGRect(origin:CGPoint.zero, size:image.size), operation: NSCompositingOperation.copy, fraction: 1)
        //viewContext.draw(self.image, in: self.bounds, byTiling:false)
        }

        viewContext.setFillColor(CGColor.clear)
        viewContext.setLineWidth(1.0)
        let selectionRectColors = [ CGColor(red: 1, green: 0, blue: 0, alpha: 1),
                                    CGColor(red: 0, green: 1, blue: 0, alpha: 1),
                                    CGColor(red: 0, green: 0, blue: 1, alpha: 1)]
        //let rmFrameColor = CGColor.init(gray: 0.5, alpha: 1)
        for i in 0..<self.selectionRects.count {
            viewContext.setStrokeColor(selectionRectColors[i % selectionRectColors.count])

            let rmFrameRect = getRmFrameRect(forSelectionRect: self.selectionRects[i])
            //viewContext.setStrokeColor(rmFrameColor)
            viewContext.setLineWidth(1.0)
            viewContext.stroke(rmFrameRect)

            viewContext.setStrokeColor(selectionRectColors[i % selectionRectColors.count])
            viewContext.setLineWidth(2.0)
            viewContext.stroke(self.selectionRects[i])
        }
            
    }
    
    func getRmFrameRect(forSelectionRect selectionRect: CGRect) -> CGRect {
        
        let rectSize = selectionRect.size
        let rmAvailableSize = CGSize(width: RemarkableTabletFormatter.kRmWidthMM-self.writingAreaMm-self.hMarginMm,
                                     height: RemarkableTabletFormatter.kRmHeightMM)
        let rmAspect = rmAvailableSize.width / rmAvailableSize.height
        let rectAspect = rectSize.width / rectSize.height
        
        let fitRectSize = ((rectAspect > rmAspect) ?
            CGSize(width: rectSize.width, height: rectSize.width / rmAspect) :
                            CGSize(width: rectSize.height * rmAspect, height: rectSize.height))
        let frameSize = CGSize(width: fitRectSize.width + (self.writingAreaMm/RemarkableTabletFormatter.kRmWidthMM + self.hMarginMm/RemarkableTabletFormatter.kRmWidthMM) * fitRectSize.height,
                               height: fitRectSize.height)

        let deltaHeight = frameSize.height - rectSize.height
        let deltaY = -deltaHeight/2
        let deltaX = -self.hMarginMm/RemarkableTabletFormatter.kRmWidthMM*fitRectSize.height
        return CGRect(origin: CGPoint(x: selectionRect.origin.x + deltaX,
                                      y: selectionRect.origin.y + deltaY),
                      size: frameSize)
    }

    override func mouseUp(with event: NSEvent) {
        raiseSelectionRectChanged()
    }
    
    override func mouseDown(with event: NSEvent) {
        let localPos = self.convert(event.locationInWindow, from: self.window!.contentView)
        let (translation, scale) = getTranslateAndScale()
        let scaledPos = CGPoint(x: (localPos.x-translation.x)/scale, y: (localPos.y-translation.y)/scale)
        //print(localPos)
        let threshold = CGFloat(20)/scale
        (self.activeSelectionRect, self.draggingEdge) = PDFMergedPagesView.findDraggingEdgeForMouseDown(scaledPos, self.selectionRects, threshold: threshold)
    }
    
    static func findDraggingEdgeForMouseDown(_ scaledPos: CGPoint, _ selectionRects: [CGRect], threshold:CGFloat) -> (Int, DraggingEdge)
    {
        var bestRect = -1
        var bestEdge = DraggingEdge.none
        var bestDistance = CGFloat.greatestFiniteMagnitude
        let kCornerThresh = threshold
        for i in 0..<selectionRects.count {
            let selectionRect = selectionRects[i]
            let activeArea = selectionRect.insetBy(dx: -threshold, dy: -threshold)
            if !activeArea.contains(scaledPos) {
                continue
            }
            let dxMin = abs(scaledPos.x - selectionRect.minX)
            let dxMax = abs(scaledPos.x - selectionRect.maxX)
            let dyMin = abs(scaledPos.y - selectionRect.minY)
            let dyMax = abs(scaledPos.y - selectionRect.maxY)
            let dBl = sqrt(dxMin*dxMin + dyMin*dyMin)
            let dBr = sqrt(dxMax*dxMax + dyMin*dyMin)
            let dTl = sqrt(dxMin*dxMin + dyMax*dyMax)
            let dTr = sqrt(dxMax*dxMax + dyMax*dyMax)

            if dxMin < bestDistance {
                bestRect = i
                bestDistance = dxMin
                bestEdge = .left
            }
            if dxMax < bestDistance {
                bestRect = i
                bestDistance = dxMax
                bestEdge = .right
            }
            if dyMin < bestDistance {
                bestRect = i
                bestDistance = dyMin
                bestEdge = .bottom
            }
            if dyMax < bestDistance {
                bestRect = i
                bestDistance = dyMax
                bestEdge = .top
            }
            // corners
            if dBl < kCornerThresh {
                bestRect = i
                bestDistance = dBl
                bestEdge = .bottomLeft
            }
            if dTl < kCornerThresh {
                bestRect = i
                bestDistance = dTl
                bestEdge = .topLeft
            }
            if dBr < kCornerThresh {
                bestRect = i
                bestDistance = dBr
                bestEdge = .bottomRight
            }
            if dTr < kCornerThresh {
                bestRect = i
                bestDistance = dTr
                bestEdge = .topRight
            }
        }
        
        if bestDistance > threshold {
            bestEdge = .none
        }
        
        return (bestRect, bestEdge)
    }
    
    override func mouseDragged(with event: NSEvent) {
        let (_, scale) = getTranslateAndScale()
        if self.activeSelectionRect == -1 {
            return
        }
        let scaledDelta = CGPoint(x: event.deltaX/scale, y: event.deltaY/scale)
        var origin = self.selectionRects[self.activeSelectionRect].origin
        var size = self.selectionRects[self.activeSelectionRect].size

        switch self.draggingEdge {
        case .left, .topLeft, .bottomLeft:
            size.width -= scaledDelta.x
            origin.x += scaledDelta.x
            break
            
        case .right, .topRight, .bottomRight:
            size.width += scaledDelta.x
            break

        case .none:
            origin.x += scaledDelta.x
            break
            
        default:
            break
        }
        
        switch self.draggingEdge {
        case .bottom, .bottomLeft, .bottomRight:
            size.height += scaledDelta.y
            origin.y -= scaledDelta.y
            break
            
        case .top, .topLeft, .topRight:
            size.height -= scaledDelta.y
            break
            
        case .none:
            origin.y -= scaledDelta.y
            break
            
        default:
            break
        }
        
        let scaledBounds = CGRect(x:0, y:0, width:self.bounds.size.width/scale, height:self.bounds.size.height/scale)
        self.selectionRects[self.activeSelectionRect] = RectUtil.clampRect(CGRect(origin: origin, size: size), bounds: scaledBounds)
        self.setNeedsDisplay(self.bounds)
    }
    
    
    func getTranslateAndScale() -> (CGPoint, CGFloat) {
        
        if let image = self.image {
            let padding = image.size.height * 0.05
            let imageSizePadded = NSSize(width: image.size.width+2*padding, height: image.size.height+2*padding)
            let (translate, scale) = RectUtil.translateAndScaleToFit(sourceSize: imageSizePadded, targetSize: self.bounds.size)
            return (CGPoint(x: translate.x + scale*padding, y: translate.y + scale*padding), scale)
        } else {
            return (CGPoint.zero, CGFloat(1))
        }
        
    }
    
    
    
    func raiseSelectionRectChanged()
    {
        onSelectionRectChanged?()
    }
    
    static func makeImageForDocument(_ doc: PDFDocument) -> CGImage?
    {
        let size = getLargestBox(doc: doc, for: PDFDisplayBox.cropBox)
        if let imageContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: 8, bytesPerRow: Int(size.width)*4, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue) {
            imageContext.setFillColor(CGColor.white)
            imageContext.fill(CGRect(origin:CGPoint.zero, size:size))
            imageContext.setBlendMode(CGBlendMode.multiply)
            imageContext.setAlpha(0.5)
            
            let onlyAddPage2 = false
            for pageNumber in 0..<doc.pageCount {
                if onlyAddPage2 && pageNumber != 2 {
                    continue
                }
                let page = doc.page(at: pageNumber)!
                page.draw(with: PDFDisplayBox.cropBox, to: imageContext)
            }
            return imageContext.makeImage()
        } else {
            return nil
        }
    }
    
    static func getLargestBox(doc: PDFDocument, for boxType:PDFDisplayBox) -> CGSize {
        var sz = CGSize.zero
        
        for pageNumber in 0..<doc.pageCount {
            let page = doc.page(at: pageNumber)!
            let pageSize = getPageSize(page, for:boxType)
            sz.width = max(sz.width, pageSize.width)
            sz.height = max(sz.height,pageSize.height)
        }
        return sz
    }
    
    static func getPageSize(_ page: PDFPage, for boxType:PDFDisplayBox) -> CGSize {
        let pageBounds = page.bounds(for: boxType)
        if (page.rotation > (90-45) && page.rotation < (90+45)) ||
            (page.rotation > (90-45+180) && page.rotation < (90+45+180)) {
            return CGSize(width:pageBounds.size.height, height:pageBounds.size.width)
        } else {
            return CGSize(width:pageBounds.size.width, height:pageBounds.size.height)
        }
        
    }
    
    
}
