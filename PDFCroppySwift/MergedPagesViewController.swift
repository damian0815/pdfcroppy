//
//  MergedPagesViewController.swift
//  PDFCroppySwift
//
//  Created by Damian Stewart on 07/11/2018.
//  Copyright © 2018 Damian Stewart. All rights reserved.
//

import Foundation
import Quartz

class MergedPagesViewController: NSViewController {

    @IBOutlet weak var mergedPagesView: PDFMergedPagesView!
    var originalDocument : PDFDocument?
    
    @IBOutlet weak var toolbarView: NSView!

    @IBOutlet weak var twoUpButton: NSButton!
    
    var avoidToolstripCheckBox: NSButton?
    var writingAreaWidthSlider: NSSlider?
    var rotateCheckbox: NSButton?
	
	private let operationQueue = OperationQueue()
    
    var writingAreaWidthSliderRebuildTimer: Timer?

    var onCroppedPdfRebuilt : ((PDFDocument, _ hMargin : Bool, _ writingAreaWidth:CGFloat) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        rotateCheckbox = NSButton(checkboxWithTitle: "Rotate", target: self, action: #selector(rotateCheckBoxChanged))
        rotateCheckbox!.frame = alignFrame(rotateCheckbox!.frame.size, rightOf: twoUpButton.frame, hOffset: 5)
        rotateCheckbox!.intValue = 0
        self.toolbarView.addSubview(rotateCheckbox!)
        
        avoidToolstripCheckBox = NSButton(checkboxWithTitle: "Avoid toolstrip", target: self, action:
            #selector(horizontalMarginCheckBoxChanged))
        avoidToolstripCheckBox!.frame = alignFrame(avoidToolstripCheckBox!.frame.size, rightOf: rotateCheckbox!.frame, hOffset: 30)
        avoidToolstripCheckBox!.intValue = 0
        self.toolbarView.addSubview(avoidToolstripCheckBox!)
                
        writingAreaWidthSlider = NSSlider(value: 22, minValue: 0, maxValue: 100, target: self, action: #selector(writingAreaWidthSliderChanged))
        writingAreaWidthSlider!.frame = alignFrame(writingAreaWidthSlider!.frame.size, rightOf: avoidToolstripCheckBox!.frame, hOffset:30)
        self.toolbarView.addSubview(writingAreaWidthSlider!)

        self.propagateSliderValues()
        
        self.mergedPagesView.onSelectionRectChanged = {
            self.cropPdf()
        }
    }
    

    func propagateSliderValues() {
        self.writingAreaWidthSliderChanged(writingAreaWidthSlider as Any)
        self.horizontalMarginCheckBoxChanged(avoidToolstripCheckBox as Any)
    }
    
    func cropPdf()
    {
        guard let originalDocument = originalDocument else {
            return
        }
        if writingAreaWidthSliderRebuildTimer != nil {
            writingAreaWidthSliderRebuildTimer?.invalidate()
            writingAreaWidthSliderRebuildTimer = nil
        }
        
        
        // rect is relative to the crop box of each page
        //let buildOp = BuildCroppedPDFOperation(document: self.originalDocument!, cropBoxRelativeRects: self.mergedPagesView.selectionRects)
		let croppedPdf = BuildCroppedPDFOperation.buildCroppedPdf(from: originalDocument, cropBoxRelativeRects: self.mergedPagesView.selectionRects, isCancelled: { return false })
		let makeToolstripMargin = self.avoidToolstripCheckBox!.intValue > 0
		let writingAreaWidth = self.writingAreaWidthSlider!.floatValue
		print("writingAreaWidth:", writingAreaWidth)

		//buildOp.completionBlock = {
		//	if let croppedPdf = buildOp.result {
		if let croppedPdf = croppedPdf {
		//		DispatchQueue.main.async {
					self.onCroppedPdfRebuilt?(croppedPdf, makeToolstripMargin, CGFloat(writingAreaWidth))
		//		}
		}
		//}
		//self.operationQueue.addOperation(buildOp)
		
    }
    
    func setPdfDocument(doc: PDFDocument) {
        originalDocument = doc
        updateDocument()
        //cropPdf()
    }
    
    func updateDocument() {
        if (rotateCheckbox!.intValue == 0) {
            mergedPagesView.setPdfDocument(doc: originalDocument!)
        } else {
            let rotatedDocument = MergedPagesViewController.rotateDocument(doc:originalDocument!, angle:90);
            mergedPagesView.setPdfDocument(doc: rotatedDocument)
        }
    }
    
    static func rotateDocument(doc:PDFDocument, angle:Int) -> PDFDocument {
        let rotatedDoc = PDFDocument()
        for pageNum in 0..<doc.pageCount {
            let page = doc.page(at: pageNum)!.copy() as! PDFPage
            page.rotation = page.rotation + angle
            rotatedDoc.insert(page, at: rotatedDoc.pageCount)
        }
        return rotatedDoc;
    }
    
    func alignFrame(_ size: CGSize, rightOf: CGRect, hOffset: CGFloat) -> CGRect {
        let ox = rightOf.maxX + hOffset
        let cy = rightOf.midY
        let oy = cy - size.height/2
        return CGRect(x: ox, y: oy, width: size.width, height: size.height)
    }
    
    
    @IBAction func twoUpCheckBoxChanged(_ sender: Any) {
        let bt = sender as! NSButton
        print("2-up:",bt.intValue)
        
        let numSelectionRects = (bt.intValue == 0) ? 1 : 2
        
        self.mergedPagesView.setNumSelectionRects(numSelectionRects)
        cropPdf()
    }
    
    @objc func horizontalMarginCheckBoxChanged(_ sender: Any) {
        self.mergedPagesView.hMarginMm = CGFloat(self.avoidToolstripCheckBox!.intValue > 0 ? (RemarkableTabletFormatter.kHUIMarginMM) : 0)
        cropPdf()
    }
    
    @objc func verticalMarginCheckBoxChanged(_ sender: Any) {
        self.mergedPagesView.needsDisplay = true
        cropPdf()
    }
    
    @objc func writingAreaWidthSliderChanged(_ sender: Any) {
        if writingAreaWidthSliderRebuildTimer != nil {
            writingAreaWidthSliderRebuildTimer?.invalidate()
        }
        
        self.mergedPagesView.writingAreaMm = CGFloat(writingAreaWidthSlider!.floatValue)
        
        writingAreaWidthSliderRebuildTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: {_ in
            self.cropPdf()
        })
    }
    
    @objc func rotateCheckBoxChanged(_ sender: Any) {
        updateDocument()
        self.mergedPagesView.needsDisplay = true
        cropPdf()
    }
    
}
